## CSS Essential Notes
#### CSS helps presentation layer, makes style for html
---
* Some sources to generate text file:  
  * https://unslash.it  
  * https://metheipsums.com  

There are 3 ways to style css in html  
1. External CSS  
  * Extension: .css  
  * always referenced within the ```<head>```
  * uses the ```<link>``` and rel, href to load  
  ```html
  <link rel="stylesheet" href="<name>.css">  
  ```
  > required for previous html  
  ```html
  <link rel="stylesheet" type="text/css" href="<name>.css">  
  ```  
2. Inline CSS  
  * Inline CSS is added using the **style** atribute.  
  ```html  
  <p style="color: blue;">  
  ```  
  * Inline CSS *takes precedence* over external CSS.  
3. Internal CSS  
  * Internal CSS will take precedence, only if it's added after external stylesheet.  
  * put **style** block inside ```<head>```  

File Naming Tips  
* Put meaning name for file  
* Using Underscore, ex: file_landing.html  
* Using dash, ex: file-landing.html (best for SEO)  

### CSS Terminology and Syntax  
**Selectors**: determine which HTML element to apply styles to.  
**Declaration blocks** consist of one or more styles rules, enclosed in curly braces ```{}```.  
**Declarations**: style rules, written in ```property:value;``` pairs.  
**Properties**: determine the type of style.  
**Values**: specific to the property and vary depending on the property.  

Formatting CSS:  
* Use whitespace to make the CSS easier to read  

### CSS Selector
1. Type Selector: match the HTML by using the element name.  
2. Class and ID Attributes: attach extra information to HTML elements.  
  * Class Slector:  
    * The same class can be used multiple times per page.  
    * The class value is the selector, starting nwith a period.  
  * Multiple Classes:  
    * Separate mutiple classes with a space  
    * Apply different styles to each class, separately 
    * Combine classes, with no space to select both   
  * ID Selector:  
    * IDs can only be used once per page.  
    * The value is the selector, starting with a # symbol.  

3. Types:  
**Descandent Selector**  
  * Use multiple selectors, separated by a space, to match the descendant elements  
  Ex: 
  ```html  
  <p> Paragraph with a <a href="#">link</a></p>  
  ```  
  => p a {}  
    
**Group mutiple selector**  
  * each selector in the group is independent of each other  
  ex: h1, h2 {}  

**Pseudo-Class Selectors**  
  * specify a state of the element  
  * keywords are combined with another selector, using a colon  
  ex: a:hover {}  

**NOTE**  
* Type Selector: Used to select all or most instances of an element.  
* Class Selector: Used fo more specific styles that can also be applied to diffrent elements, one ore more times per page.  
* ID Selector: ID values can only used once per page; use them for unique or global styles that are not repeated.  
* Descendant Slectors: 
  * Used to apply styles to an element, based on its parent or ancestor element.  
  * Avoid going more than three levels deep. 
* Combining Selectors: Combine selectors to apply the same style to different elements, in one declaration block.  

### CSS Color Values
* **keywords**: use the actual name
* **hex code**: number sign (#) followed by six characters (0-9, A-F)
* **RGB**: red, green, and blue values on a scale between 0 and 255

**NOTE**  
#### CSS Specific Ranking
* Specificity point system
style atrribute > id > class, attribule, psuedo class > element
* **Calculate**:  
element (1 point) 
+class/attribute/psuedo-class (10 points)   
+id(100 points)   
+style attribute (1000 points)  
*!import = unlimited points*

### Typography
* **Typography**: the study of the design and use of type for communication
* **Typeface**: a set of fonts, designed with common characteristics and composed of glyphs  
* **Font**: individual files that are part of a typeface  

* Serif and Sans Serif Typefaces
  * Serif typefaces have small decorative lines.
  * Sans serif typefaces have no decorative lines.  
* Script and Decorative Typefaces:  
  * Script typefaces: have a hand-lettered look.  
  * Decorative typefaces are distinct and ornamental.  
* Monospace Typeface  
  In monospace typefaces, each character uses the same amount of horizontal space.  

**ADD FONT**
- font-family
- web font

Font Size:
* px
	* measure screens in pixels
	* absolute value; greate for accuracy
	* use whole numbers and avoid decimals
	* browser default = 16px
* em
	* named after the letter "m"
	* relative unit
	* 1em = inherited font-size
	* if no font-size is declared, 1em = default = 16px
* rem
	* newer unit, which is similar to "em"
	* if no font-size is declared, 1rem = 1em = default = 16px
	* relative unit but only to the root element (html)

font-weight
* thickness or boldness of typefaces
* number values are 100, 200, ... 900
* number values will map to the nearest typefacea available
normal = 400, bold = 700
font-style
* used to add or remove an italic style
* three values: italic, oblique and normal
text-decoration
* used to remove or add an underline above, below, or through the text
	* overline
	* underline
	* line-through
	* none
text-transform
* specific the letter casing
* the values are capitial, uppercase, lowercase, none
text-align
* the values: center, left, right

### HTML Elements
Block and Inline
* Block Elements
	* Height = content
	* Width = 100% of container
	* Elements start on a new line
	* Block elements can wrap other block and inline elements.
	* h1...h6, div, p
* Inline element
	* Height and width = content
	* Elements align left, in a line.
	* Inline elements can only nest other inline element (except <a> tags in HTML5)
	* a, span, strong
Box Model
* Width and height: sets specific size for the content box
* Padding: space inside of the element
* Margin: space outside of the element
* Border: displays between the padding and margin

Padding Shorthand Syntax
```css
padding: 2px; /* same on all sides */
padding: 2px 10px; /* top & bottom, right & left */
padding: 2px 10px 5px; /* top, right & left, bottm */
padding: 2px 10px 5px 2px /* top, right, bottom, left */
```
Float  
* syntax: ```float: left/right;```  
* clear float: ```clear: both;```  
* Note:
  * The parent elements do not recoginze the height of floated elements and will only wrap around nonfloated child elements.
  * If all the elements within the parent are floated, the height of the parent container will collapse.
  * When there no elements to apply a clear to, the parent element will need to self-clear.
* Self-Clearning Floats: Overflow
  * syntax: ```overflow: auto``` (add scrollbar) or ```overflow:hidden```  
* Self-Clearing Floats: "Clearfix" Hack  
  * add class clearfix  
```css  
.clearfix:after {
  content: '';  
  display: table;  
  clear: both;    
}  
```
  * link: css-tricks.com  
* Using flexbox  
Box-sizing  
  * how to browser calculate the height and width  
  * default: content-box (add padding and margin)  
  * border-box (no effect by padding and border)   



